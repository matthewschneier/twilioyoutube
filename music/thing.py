"""Test video download and conversion."""
from pytube import YouTube
from os import listdir, makedirs, rename
from os.path import isfile, join
from subprocess import Popen

link = input("Enter video link: ")
filepath = "/home/mschneier/Music/" + link[-5:]
yt = YouTube(link)
videos = yt.fmt_streams

makedirs(filepath)

video = videos[0]
video.download(filepath)

onlyfiles = [
    f for f in listdir(filepath)
    if isfile(join(filepath, f))
]

rename(
    filepath + "/" + onlyfiles[0],
    filepath + "/" + onlyfiles[0].replace(" ", "")
)

onlyfiles = [
    f for f in listdir(filepath)
    if isfile(join(filepath, f))
]

Popen(["ffmpeg", "-i", filepath + "/" + onlyfiles[0],
       filepath + "/" + onlyfiles[0][:-4] + ".mp3"])

