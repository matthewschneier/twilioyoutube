from flask import Flask, Response, request
from pytube import YouTube
from os import listdir, makedirs, rename
from os.path import isfile, join
import shutil
from subprocess import Popen
from twilio import twiml

app = Flask(__name__)

@app.route("/")
def check_app():
    """Returns a simple string stating the app is working."""
    return Response("It works!"), 200


@app.route("/test/")
def testing():
    return Response("Hi."), 200


@app.route("/text/", methods=["GET", "POST"])
def inbound_sms():
    """Download video on text message."""
    videoURL = request.form.get("Body")
    link = "https://youtu.be.com/" + videoURL
    filepath = "/home/mschneier/Music/" + link[-11:]
    yt = YouTube(link)
    videos = yt.fmt_streams
    makedirs(filepath)
    video = videos[0]
    video.download(filepath)
    onlyfiles = [
        f for f in listdir(filepath)
        if isfile(join(filepath, f))
    ]
    rename(
        filepath + "/" + onlyfiles[0],
        filepath + "/" + onlyfiles[0].replace(" ", "")
    )
    onlyfiles = [
        f for f in listdir(filepath)
        if isfile(join(filepath, f))
    ]
    Popen(["ffmpeg", "-i", filepath + "/" + onlyfiles[0],
           filepath + "/" + onlyfiles[0][:-4] + ".mp3"])

if __name__ == "__main__":
    app.run(debug=True)
    
