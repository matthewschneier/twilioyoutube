# TwilioYoutube
Use Twilio, Ngrok, and Flask to download YouTube video and convert it into an .mp3 file.

1. Sign up for Twilio account.
2. Sign up for Ngrok account.
3. Instal python packages (pip install -r requirements.txt).
4. Start webserver (python app.py).
5. Have Ngrok point to your Flask server.
6. Point Twilio at Ngrok.
